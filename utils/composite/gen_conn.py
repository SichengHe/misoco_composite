import numpy as np



def gen_conn(N):

    conn = []

    for i in xrange(N):

        if i==N-1:

            for j in xrange(N):

                elem_ind = i*N + j

                if j==N-1:

                    pass

                else:

                    top_ind = elem_ind + 1

                    conn.append([elem_ind, top_ind])

        else:

            for j in xrange(N):

                elem_ind = i*N + j

                if j==N-1:

                    right_ind = elem_ind + N

                    conn.append([elem_ind, right_ind])

                else:

                    top_ind = elem_ind + 1
                    right_ind = elem_ind + N

                    conn.append([elem_ind, top_ind])
                    conn.append([elem_ind, right_ind])


    return conn 

        