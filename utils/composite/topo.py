# Sicheng He
# 11:30am 1/31/2018

import numpy as np
import os


# generate square geometry
def topology_generator_sqr(N, l, dir):

    # generator the: elem, node, BC file for a N-by-N square geometry
    # input: N: element number per side
    #        l: lengh the edge of one square element 

    node_perside = (N-1)*2+3

    # element data
    elem = []

    for i in xrange(N):
        for j in xrange(N):

            elem_loc = np.zeros(9,dtype=np.int16)

            row_index_0 = 2*i
            row_index = [row_index_0,row_index_0+1,row_index_0+2]

            col_index_0 = 2*j
            col_index = [col_index_0,col_index_0+1,col_index_0+2]

            for p in xrange(3):
                for q in xrange(3):
                    node_index = row_index[p]*node_perside+col_index[q]
                    elem_loc[3*p+q] = node_index
            elem.append(elem_loc)

    # boundary nodes index
    BC = []
    for i in xrange(node_perside):
        BC.append(i)

    node_N = node_perside**2

    # node coordinates data
    half_l = l*0.5
    node = np.zeros((node_N, 3))
    for i in xrange(node_perside):
        for j in xrange(node_perside):

            ind = i*node_perside + j

            node[ind, 0] = half_l*i
            node[ind, 1] = half_l*j
            node[ind, 2] = 0.0
    try:
        # try to make the input directory
        os.makedirs(dir)
    except:
        # if exists, do nothing
        pass
    np.savetxt(dir+'/'+'node.txt', node)
    np.savetxt(dir+'/'+'elem.txt', elem)
    np.savetxt(dir+'/'+'BC.txt', BC)



testFlag = False
if testFlag:
    topology_generator(3, 1.0/3)


# # generate rectangular geometry
# def topology_generator_rec(Nx, Ny, l, dir):

#     # generator the: elem, node, BC file for a Nx-by-Ny rectangular geometry
#     # input:  Nx: number of element in x direction
#     #         Ny: number of element in y direction
#     #          l: lengh the edge of one square element 
#     #        dir: directory to store the data

#     #                    col:
#     #      ______________
#     #    /|_|_|_|_|_|_|_| 3 
#     #    /|_|_|_|_|_|_|_| 2
#     #    /|_|_|_|_|_|_|_| 1 
#     # row: 1 2 3 4 5 6 7
#     # Nx = 7, Ny = 3











