import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def postprocess(elem, node, disp, subset):

	fig = plt.figure()
	ax = fig.gca(projection='3d')

	elem_N = len(elem)

	for i in xrange(elem_N):
		for j in xrange(len(subset)):

			loc_ind1 = subset[j][0]
			loc_ind2 = subset[j][1]

			glo_ind1 = elem[i][loc_ind1] 
			glo_ind2 = elem[i][loc_ind2]

			xvec1 = node[glo_ind1][:]
			xvec2 = node[glo_ind2][:]

			disp1 = disp[5*glo_ind1:5*glo_ind1 + 3, 0]
			disp2 = disp[5*glo_ind2:5*glo_ind2 + 3, 0]

			# before loading
			x0 = [xvec1[0], xvec2[0]]
			y0 = [xvec1[1], xvec2[1]]
			z0 = [xvec1[2], xvec2[2]]

			ax.plot(x0, y0, z0,'-g')

			# after loading
			x1 = [xvec1[0]+disp1[0], xvec2[0]+disp2[0]]
			y1 = [xvec1[1]+disp1[1], xvec2[1]+disp2[1]]
			z1 = [xvec1[2]+disp1[2], xvec2[2]+disp2[2]]

			ax.plot(x1, y1, z1,'-r')


	plt.show()

			
