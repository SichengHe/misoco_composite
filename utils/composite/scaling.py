import sys
sys.path.append('/Users/hesicheng/Desktop/test') 
sys.path.append('/Users/hesicheng/Desktop/test/utils') 

from plate_layerwise import *

import numpy as np

def scalingK(K_list):

    # Scale u
    K_avg = 0.0
    K_scaled_list = []
    for i in xrange(len(K_list)):

        K_loc = K_list[i]

        K_avg += K_loc

    K_avg /= np.float(len(K_list))
    J_invhalf = np.matrix(np.zeros((45,45)))

    for i in xrange(45):
        J_invhalf[i, i] = 1.0/np.sqrt(K_avg[i,i])

    # unification
    set1 = [0, 2, 6, 8]
    set2 = [1, 3, 5, 7]
    set3 = [4] # unchanged is fine

    sum_1 = np.diag(np.zeros(5))
    for i in xrange(len(set1)):
        index_start = set1[i]*5
        index_end = set1[i]*5 + 5

        sum_1 += J_invhalf[index_start:index_end, index_start:index_end]

    Jtype1 = sum_1/np.float(len(set1))

    for i in xrange(len(set1)):
        index_start = set1[i]*5
        index_end = set1[i]*5 + 5

        J_invhalf[index_start:index_end, index_start:index_end] = Jtype1



    sum_2 = np.diag(np.zeros(5))
    for i in xrange(len(set2)):
        index_start = set2[i]*5
        index_end = set2[i]*5 + 5

        sum_2 += J_invhalf[index_start:index_end, index_start:index_end]

    Jtype2 = sum_2/np.float(len(set2))

    for i in xrange(len(set2)):
        index_start = set2[i]*5
        index_end = set2[i]*5 + 5

        J_invhalf[index_start:index_end, index_start:index_end] = Jtype2


    Jtype3 = J_invhalf[set3[0]*5:set3[0]*5+5, set3[0]*5:set3[0]*5+5]
    J_invhalf[set3[0]*5:set3[0]*5+5, set3[0]*5:set3[0]*5+5] = Jtype3

    J_half = np.matrix(np.zeros((45,45)))
    for i in xrange(45):
        J_half[i, i] = 1.0/J_invhalf[i, i]



    for i in xrange(len(K_list)):

        K_loc = K_list[i]

        K_loc_scaled = J_invhalf.dot(K_loc).dot(J_invhalf)

        K_scaled_list.append(K_loc_scaled)

    # if 1:
    #     for i in xrange(len(K_list)):
    #         print np.linalg.cond(K_list[i][15:, 15:,]), np.linalg.cond(K_scaled_list[i][15:, 15:,])

    # # exit()

    # print "J_invhalf", np.diag(J_invhalf)


    return K_scaled_list, J_invhalf, J_half, Jtype1, Jtype2, Jtype3



def scaling_Q_B(B, QPQ_list, Qq_list, r_list, J_invhalf):

    # r_list not really used

    B_scaled_list = []
    Q_large_scaled_list = [] 

    for i in xrange(len(QPQ_list)):

        QPQ_loc = QPQ_list[i]
        Qq_loc = Qq_list[i]

        eig_val, eig_vec = np.linalg.eig(QPQ_loc)
        eig_val = np.diag(eig_val)

        eig_val_sqrt = np.zeros((eig_val.shape[0], eig_val.shape[0]))
        eig_val_inv_sqrt = np.zeros((eig_val.shape[0], eig_val.shape[0]))
        for j in xrange(eig_val.shape[0]):
            eig_val_sqrt[j, j] = np.sqrt(eig_val[j, j])
            eig_val_inv_sqrt[j, j] = 1.0/eig_val_sqrt[j, j]

        B_loc = eig_val_sqrt.dot(np.transpose(eig_vec)).dot(B).dot(J_invhalf)
        B_scaled_list.append(B_loc)

        qbar = eig_val_inv_sqrt.dot(np.transpose(eig_vec)).dot(Qq_loc)


        Q_large = np.zeros((4, 4))
        Q_large[:3, :3] += np.eye(3)
        Q_large[:3, 3:4] += qbar
        Q_large[3:4, :3] += np.transpose(qbar)
        Q_large[3, 3] += -1

        # for ii in xrange(4):
        #     for jj in xrange(4):
        #         if abs(Q_large[ii, jj])<=1e-12:
        #             Q_large[ii, jj] = 0.0

        Q_large_scaled_list.append(Q_large)

    print "Q_large_scaled_list", Q_large_scaled_list

    return Q_large_scaled_list, B_scaled_list


def scaling_f(f, node_type_list, Jtype1, Jtype2, Jtype3):

    node_N = len(node_type_list)

    Jtype_list = [Jtype1, Jtype2, Jtype3]

    f_array = (np.asarray(f)).reshape((5*node_N, 1))
    f_scaled_array = np.zeros((5*node_N, 1))

    for i in xrange(node_N):

        type_loc = node_type_list[i]
        Jtype_loc = Jtype_list[type_loc]

        f_scaled_array[5*i: 5*i+5, 0] = Jtype_loc.dot(f_array[5*i: 5*i+5, 0])

    f_scaled = (f_scaled_array.reshape(5*node_N)).tolist()

    return f_scaled


def scaling_ub_lb(lb, ub, node_type_list, Jtype1, Jtype2, Jtype3):

    node_N = len(node_type_list)

    Jtype1_pos = np.zeros((5, 5))
    Jtype2_pos = np.zeros((5, 5))
    Jtype3_pos = np.zeros((5, 5))

    for i in xrange(5):

        Jtype1_pos[i, i] = 1.0/Jtype1[i, i]
        Jtype2_pos[i, i] = 1.0/Jtype2[i, i]
        Jtype3_pos[i, i] = 1.0/Jtype3[i, i]

    Jtype_pos_list = [Jtype1_pos, Jtype2_pos, Jtype3_pos]

    lb_scaled_list = []
    ub_scaled_list = []

    for i in xrange(node_N):

        lb_loc = (np.asarray(lb[i])).reshape(5, 1)
        ub_loc = (np.asarray(ub[i])).reshape(5, 1)

        type_loc = node_type_list[i]
        Jtype_loc = Jtype_pos_list[type_loc]

        lb_scaled_loc = Jtype_loc.dot(lb_loc)
        ub_scaled_loc = Jtype_loc.dot(ub_loc)

        lb_scaled_loc = (lb_scaled_loc.reshape(5)).tolist()
        ub_scaled_loc = (ub_scaled_loc.reshape(5)).tolist()


        lb_scaled_list.append(lb_scaled_loc)
        ub_scaled_list.append(ub_scaled_loc)


    return lb_scaled_list, ub_scaled_list









