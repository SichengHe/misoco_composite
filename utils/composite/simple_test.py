import numpy as np

A = np.zeros((3, 3))
A[0, 0] = 1.0
A[1, 1] = 1.0
A[0, 2] = 1.0
A[1, 2] = 2.0
A[2, 0] = 1.0
A[2, 1] = 2.0
A[2, 2] = -1.0


B = np.zeros((2, 2))
B[0, 0] = 2.0
B[0, 1] = 2.0
B[1, 0] = 2.0
B[1, 1] = 5.0


eigval_A, eigvec_A = np.linalg.eig(A)
eigval_B, eigvec_B = np.linalg.eig(B)

# assume first eigvalue of A negative or error
if eigval_A[0]>=0:
	print "Error:assume first eigvalue of A negative!"
	exit()

	
print "eigval_A", eigval_A, "eigval_B", eigval_B
print "eigvec_A", eigvec_A, "eigvec_B", eigvec_B

