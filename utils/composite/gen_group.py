import numpy as np


def gen_group(N_elem, N_patch):

    # N_elem element per edge
    # N_patch patch per edge

    elem_per_edge_per_patch = N_elem/N_patch


    patchedElem_list = []

    for i in xrange(N_patch):
        for j in xrange(N_patch):
            patchedElem_list.append([])


    range_list = []

    for i in xrange(N_patch):

        # U = [lb1, ub1] U [lb2, ub2] U ... U [lbn, ubb]

        lb = i*elem_per_edge_per_patch
        ub = (i+1)*elem_per_edge_per_patch - 1

        range_list.append([lb, ub])


    def get1stTrueInd(trueFalse_list):

        for i in xrange(len(trueFalse_list)):
            if trueFalse_list[i]:
                return i


    for i in xrange(N_elem):
        for j in xrange(N_elem):

            elem_ind = i*N_elem + j

            trueFalse_list_i = map(lambda x: x[0]<=i and x[1]>=i, range_list)
            trueFalse_list_j = map(lambda x: x[0]<=j and x[1]>=j, range_list)


            patch_ind_i = get1stTrueInd(trueFalse_list_i)
            patch_ind_j = get1stTrueInd(trueFalse_list_j)

            patchedElem_list[patch_ind_i*N_patch + patch_ind_j].append(elem_ind)


    return patchedElem_list