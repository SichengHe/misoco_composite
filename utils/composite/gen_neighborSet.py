import numpy as np



def gen_neighborSet(N):

    # assume square

    #   _____
    # /|5_|11|
    # /|4_|10|
    # /|3_|9_|
    # /|2_|8_|
    # /|1_|7_|
    # /|0_|6_|
    #

    neighborSet= []

    for i in xrange(N):

        if i==0:

            for j in xrange(N):

                elem_ind = i*N + j

                neighborSet_loc_center = elem_ind
                neighborSet_loc_neighbors = []

                if j==0:

                    neighborSet_loc_neighbors.append(neighborSet_loc_center + 1)
                    neighborSet_loc_neighbors.append(neighborSet_loc_center + N)

                elif j==N-1:

                    neighborSet_loc_neighbors.append(neighborSet_loc_center - 1)
                    neighborSet_loc_neighbors.append(neighborSet_loc_center + N)

                else:

                    neighborSet_loc_neighbors.append(neighborSet_loc_center - 1)
                    neighborSet_loc_neighbors.append(neighborSet_loc_center + 1)
                    neighborSet_loc_neighbors.append(neighborSet_loc_center + N)

                neighborSet_loc = [neighborSet_loc_center, neighborSet_loc_neighbors]
                neighborSet.append(neighborSet_loc)

        elif i==N-1:

            for j in xrange(N):

                elem_ind = i*N + j

                neighborSet_loc_center = elem_ind
                neighborSet_loc_neighbors = []

                if j==0:

                    neighborSet_loc_neighbors.append(neighborSet_loc_center + 1)
                    neighborSet_loc_neighbors.append(neighborSet_loc_center - N)

                elif j==N-1:

                    neighborSet_loc_neighbors.append(neighborSet_loc_center - 1)
                    neighborSet_loc_neighbors.append(neighborSet_loc_center - N)

                else:

                    neighborSet_loc_neighbors.append(neighborSet_loc_center - 1)
                    neighborSet_loc_neighbors.append(neighborSet_loc_center + 1)
                    neighborSet_loc_neighbors.append(neighborSet_loc_center - N)

                neighborSet_loc = [neighborSet_loc_center, neighborSet_loc_neighbors]
                neighborSet.append(neighborSet_loc)

        else:

            for j in xrange(N):

                elem_ind = i*N + j

                neighborSet_loc_center = elem_ind
                neighborSet_loc_neighbors = []

                if j==0:

                    neighborSet_loc_neighbors.append(neighborSet_loc_center + 1)
                    neighborSet_loc_neighbors.append(neighborSet_loc_center - N)
                    neighborSet_loc_neighbors.append(neighborSet_loc_center + N)

                elif j==N-1:

                    neighborSet_loc_neighbors.append(neighborSet_loc_center - 1)
                    neighborSet_loc_neighbors.append(neighborSet_loc_center - N)
                    neighborSet_loc_neighbors.append(neighborSet_loc_center + N)

                else:

                    neighborSet_loc_neighbors.append(neighborSet_loc_center - 1)
                    neighborSet_loc_neighbors.append(neighborSet_loc_center + 1)
                    neighborSet_loc_neighbors.append(neighborSet_loc_center - N)
                    neighborSet_loc_neighbors.append(neighborSet_loc_center + N)

                neighborSet_loc = [neighborSet_loc_center, neighborSet_loc_neighbors]
                neighborSet.append(neighborSet_loc)

    return neighborSet

        