import sys
sys.path.append('/Users/hesicheng/Desktop/test') 
sys.path.append('/Users/hesicheng/Desktop/test/utils') 

import numpy as np

import comp_solver as csolver

import postprocess as postprocess

import scaling as scaling

import matplotlib.pyplot as plt 

# size
l = 1.0/3
h = 0.01


# local stiffness matrix
scale_material = 1

E1   = 1.4686*1e11*scale_material
E2   = 1.062*1e10*scale_material
G12  = 5.45*1e6*scale_material
G23  = 3.99*1e6*scale_material
nu12 = 0.33

Evec = [E1, E2, G12, G23, nu12]

Xt = 1.035*1e9*scale_material
Xc = 0.689*1e9*scale_material
Yt = 0.041*1e9*scale_material
Yc = 0.117*1e9*scale_material
S = 0.069*1e9*scale_material

TsaiWuvec = [Xt, Xc, Yt, Yc, S]


theta_option_list = [0, np.pi/4.0, np.pi/2.0, np.pi/4.0*3.0]
K_option_list = csolver.generateStiffnessMat(l, h, theta_option_list, Evec)
QPQ_option_list, Qq_option_list, r_option_list = csolver.generateQPQMat(l, h, theta_option_list, Evec, TsaiWuvec)
Qlarge_option_list = csolver.getLargeQ(l, h, theta_option_list, Evec, TsaiWuvec)


choice_list = [0, 0, 0, 1, 1, 1, 2, 2, 2]
# choice_list = [0, 0, 0, 0, 0, 0, 0, 0, 0]
# choice_list = [1, 1, 1, 1, 1, 1, 1, 1, 1]
# choice_list = [2, 2, 2, 2, 2, 2, 2, 2, 2]


K_loc_list = []
for i in choice_list:
	K_loc_list.append(K_option_list[i])



# node 
node = np.loadtxt("node.txt")
node_N = len(node)

# elem2node
elem = np.loadtxt("elem.txt")
elem = elem.astype(int)


# Boundary condition
BC = np.loadtxt("BC.txt")
BC = BC.astype(int)


# load
f = np.zeros((len(node)*5, 1))
f[-6*5-1, 0] = 0.1
f[-6*5-2, 0] = 0.1
f[-6*5-3, 0] = 2000.0
f[-6*5-4, 0] = 10.0
f[-6*5-5, 0] = 10.0



cs = csolver.comp_solver(K_loc_list, elem, node_N, BC, f)
cs.glo_stiffness()
cs.solve()
# print "cs.u", cs.u



subset = [[0,1], [1,2], [0,3], [2,5], [3,6], [5,8], [6,7], [7,8]]
postprocess.postprocess(elem, node, cs.u, subset)






# scale it!


K_scaled_option_list, J_invhalf, J_half, Jtype1, Jtype2, Jtype3 = scaling.scalingK(K_option_list)
K_scaled_loc_list = []
for i in choice_list:
	K_scaled_loc_list.append(K_scaled_option_list[i])

f_scaled = np.zeros((f.shape[0], 1))

Jtype_list = [Jtype1, Jtype2, Jtype3]
print "Jtype_list", Jtype_list
node_type_list_1 = [0,1,0,1,0,1,0]
node_type_list_2 = [1,2,1,2,1,2,1]
node_type_list = []
for i in xrange(4):
	node_type_list += node_type_list_1
	node_type_list += node_type_list_2
node_type_list += node_type_list_1

for i in xrange(node_N):
	Jtype_loc = Jtype_list[node_type_list[i]]
	f_scaled[i*5:(i+1)*5, 0] = Jtype_loc.dot(f[i*5:(i+1)*5, 0])

cs2 = csolver.comp_solver(K_scaled_loc_list, elem, node_N, BC, f_scaled)
cs2.glo_stiffness()
cs2.solve()

u_scaled = np.zeros((f.shape[0], 1))
for i in xrange(node_N):
	Jtype_loc = Jtype_list[node_type_list[i]]
	u_scaled[i*5:(i+1)*5, 0] = Jtype_loc.dot(cs2.u[i*5:(i+1)*5, 0])

postprocess.postprocess(elem, node, u_scaled, subset)
print("u_scaled-cs.u", u_scaled-cs.u)
# print "cs2.u", cs2.u





# stress
B = csolver.getB(l, h, theta_option_list, Evec)

strain_list = csolver.getStrain(elem, B, cs.u)

tsaiWu_slack_list = []
for i in xrange(len(elem)):

	choice_ind = choice_list[i]

	strain_decision_vec = np.zeros((4, 1))

	strain_decision_vec[:3, :1] += strain_list[i][:, 0]

	strain_decision_vec[3, 0] = 1.0

	tsaiWu_slack = np.transpose(strain_decision_vec).\
	dot(Qlarge_option_list[choice_ind]).dot(strain_decision_vec)

	tsaiWu_slack_list.append(tsaiWu_slack)






# scaled Tsai-Wu
B_option_list = []
Q_large_option_scaled_list = [] 


Q_large_option_scaled_list, B_option_list = \
scaling.scaling_Q_B(B, QPQ_option_list, Qq_option_list, r_option_list, J_invhalf)


strain_scaled_list = csolver.getStrain_scaled(elem, B_option_list, cs2.u, choice_list)

tsaiWu_scaled_slack_list = []
for i in xrange(len(elem)):

	choice_ind = choice_list[i]

	strain_decision_vec = np.zeros((4, 1))

	strain_decision_vec[:3, :1] += strain_scaled_list[i][:, 0]

	strain_decision_vec[3, 0] = 1.0

	tsaiWu_scaled_slack = np.transpose(strain_decision_vec).\
	dot(Q_large_option_scaled_list[choice_ind]).dot(strain_decision_vec)

	tsaiWu_scaled_slack_list.append(tsaiWu_scaled_slack)



print "strain_list", strain_list
print "strain_scaled_list", strain_scaled_list
print "cs.u", cs.u
print "cs2.u", cs2.u

print "tsaiWu_slack_list", tsaiWu_slack_list, "tsaiWu_scaled_slack_list", tsaiWu_scaled_slack_list
diff = [abs(tsaiWu_slack_list[i] - tsaiWu_scaled_slack_list[i]) \
for i in xrange(len(tsaiWu_scaled_slack_list))]
print "diff", diff


strain_array = np.asarray(strain_list)
strain_scaled_array = np.asarray(strain_scaled_list)

strain_array = [row.reshape(3) for row in strain_array]
strain_scaled_array = [row.reshape(3) for row in strain_scaled_array]

strain_array = np.asarray(strain_array)
strain_scaled_array = np.asarray(strain_scaled_array)

print "strain_array", strain_array
print "strain_scaled_array", strain_scaled_array


plt.hist(np.log10(abs(strain_array)+1))
plt.show()
plt.hist(np.log10(abs(strain_scaled_array)+1))
plt.show()


plt.hist(np.log10(abs(cs.u)+1))
plt.show()
plt.hist(np.log10(abs(cs2.u)+1))
plt.show()








