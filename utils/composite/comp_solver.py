# composite structure solver

import numpy as np
from plate_layerwise import *
import copy

# import matplotlib.pyplot as plt
# from mpl_toolkits.mplot3d import Axes3D
# import copy


def generateStiffnessMat(l, h, theta_list, Evec):

    # assume the length and thickness same thru all elements

    K_loc_list = []

    for i in xrange(len(theta_list)):

        theta = theta_list[i]

        local_elem = layerElement(Evec, theta, h, [l, l])

        K_loc_list.append(local_elem.stiffnessMat())

    return K_loc_list

def generateQPQMat(l, h, theta_list, Evec, TsaiWuvec):

    # assume the length and thickness same thru all elements
    # "Evec" is not really used here, just to make "layerElement" happy

    QPQ_loc_list = []
    Qq_loc_list = []
    r_loc_list = []

    Xt = TsaiWuvec[0]
    Xc = TsaiWuvec[1]
    Yt = TsaiWuvec[2]
    Yc = TsaiWuvec[3]
    S = TsaiWuvec[4]

    for i in xrange(len(theta_list)):

        theta = theta_list[i]

        local_elem = layerElement(Evec, theta, h, [l, l])
        local_elem.setTsaiWuStressParam(Xt, Xc, Yt, Yc, S)

        QPQ_loc, Qq_loc, r_loc = local_elem.setTsaiWuStrainBasic()

        QPQ_loc_list.append(QPQ_loc)
        Qq_loc_list.append(Qq_loc)
        r_loc_list.append(r_loc)

    return QPQ_loc_list, Qq_loc_list, r_loc_list

def getB(l, h, theta_list, Evec):

    local_elem = layerElement(Evec, theta_list[0], h, [l, l])

    return local_elem.center_inplane_strain()


def getStrain(elem, B, u):

    strain_list = []
    for i in xrange(len(elem)):

        u_loc = np.zeros((45, 1))
        for j in xrange(9):

            ind_glo = elem[i][j]

            u_loc[5*j:5*j+5, 0] = u[5*ind_glo:5*ind_glo+5, 0]

        strain = B.dot(u_loc)
        strain_list.append(strain)

    return strain_list


def getStrain_scaled(elem, B_option_list, u, choice_list):

    strain_list = []
    for i in xrange(len(elem)):

        choice_ind = choice_list[i]
        B = B_option_list[choice_ind]

        u_loc = np.zeros((45, 1))
        for j in xrange(9):

            ind_glo = elem[i][j]

            u_loc[5*j:5*j+5, 0] = u[5*ind_glo:5*ind_glo+5, 0]

        strain = B.dot(u_loc)
        strain_list.append(strain)

    return strain_list

def getLargeQ(l, h, theta_list, Evec, TsaiWuvec):

    # assume the length and thickness same thru all elements

    Xt = TsaiWuvec[0]
    Xc = TsaiWuvec[1]
    Yt = TsaiWuvec[2]
    Yc = TsaiWuvec[3]
    S = TsaiWuvec[4]

    Q_large_list = []

    for i in xrange(len(theta_list)):

        theta = theta_list[i]

        local_elem = layerElement(Evec, theta, h, [l, l])
        local_elem.setTsaiWuStressParam(Xt, Xc, Yt, Yc, S)

        Q_large_list.append(local_elem.setTsaiWuStrain())

    return Q_large_list





class comp_solver:

    def __init__(self, K_loc_list, elem, node_N, BC, f):


        # inputs:
        #        K_loc_list: element stiffness matrix
        #        elem: element to node indices
        #        BC: boundary element indices
        #        f: external force
        #        node_N: number of nodes

        # element stiffness matrix 
        self.K_loc_list = K_loc_list

        # elem2node mapping
        self.elem = elem
        self.elem_N = len(self.elem)

        # node number
        self.node_N = node_N

        # boundary condition
        self.BC = BC

        # load
        self.f = f


    def glo_stiffness(self):

        # generate the one layer composite local stiffness matrix

        node_N = self.node_N
        elem_N = self.elem_N
        elem = self.elem
        BC = self.BC
        K_loc_list = self.K_loc_list


        K_glo = np.matrix(np.zeros((5*node_N,5*node_N)))


        # mapping the local K to global K

        for i in xrange(elem_N):

            K_loc = K_loc_list[i]
            elem_loc = elem[i]

            # print "elem_loc", elem_loc

            for j in xrange(9):
                for k in xrange(9):

                    ind_row_glo = elem_loc[j]
                    ind_col_glo = elem_loc[k]

                    K_loc_block = K_loc[j*5:(j+1)*5,k*5:(k+1)*5]

                    K_glo[ind_row_glo*5:(ind_row_glo+1)*5,ind_col_glo*5:(ind_col_glo+1)*5] += K_loc_block

        # BC
        # implemented thru masked matrix

        mask_mat = np.zeros((5*node_N,5*node_N))
        mask_mat = (mask_mat == 1)

        for BC_loc in BC:
            for j in xrange(5):
                mask_mat[BC_loc*5+j,BC_loc*5+j] = True


        mask_mat = np.ma.masked_array(mask_mat, mask=mask_mat)
        mask_mat = np.ma.mask_rowcols(mask_mat)

        mask_mat = np.ma.getmask(mask_mat)
        mask_mat = np.invert(mask_mat)

        self.K_glo_BC = K_glo[mask_mat]
        N_row = np.int(np.sqrt(self.K_glo_BC.shape[1]))
        self.K_glo_BC = self.K_glo_BC.reshape(N_row, N_row)



    def setMask(self):

        # set the mask for load and disp

        node_N = self.node_N
        BC = self.BC

        mask_vec = np.zeros((5*node_N,1))
        mask_vec = (mask_vec==0)

        for BC_loc in BC:
            for j in xrange(5):
                mask_vec[BC_loc*5+j, 0] = False

        self.mask_vec = mask_vec


    
    def setLoad(self):

        f = self.f

        self.setMask()
        mask_vec = self.mask_vec

        f_BC = f[mask_vec]

        self.f_BC = f_BC



    def solve(self):

        node_N = self.node_N

        self.setLoad()

        K_glo_BC = self.K_glo_BC
        f_BC = self.f_BC
        mask_vec = self.mask_vec

        print "np.linalg.cond(K_glo_BC)", np.linalg.cond(K_glo_BC)

        u_BC = np.linalg.solve(K_glo_BC,f_BC)

        u = []
        j = 0
        for i in xrange(node_N*5):

            if mask_vec[i] == False:
                u.append(0.0)
            else:
                u.append(u_BC[j])
                j += 1

        self.u = np.asarray(u).reshape((len(u), 1))













