# ===========================================================
#    postprocess                            
# ===========================================================

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d

my_blue = '#4C72B0'
my_red = '#C54E52'
my_green = '#56A968' 
my_brown = '#b4943e'

class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)


def plotMesh(ax, var_name_list, var_value_list, color='k'):
    # get z & u
    z_array = []
    u_array = []
    n_var  = len(var_name_list)
    for i in xrange(n_var):

        var_name_loc = var_name_list[i]
        var_value_loc = var_value_list[i]

        if var_name_loc[0] == 'z':
            z_array.append(var_value_loc)

        if var_name_loc[0] == 'u':
            u_array.append(var_value_loc)
            
    z_array = np.asarray(z_array)
    u_array = np.asarray(u_array)
    u_array *= alpha_s


    n_node = u_array.shape[0]/5
    l_3 = l/3

    # undeformed grid pts
    x0_oneline = np.linspace(0, 1.0, 1+2*N_elem)
    x0 = np.outer(np.ones(1+2*N_elem), x0_oneline).T
    y0_oneline = np.linspace(0, 1.0, 1+2*N_elem)
    y0 = np.outer(y0_oneline, np.ones(1+2*N_elem)).T
    z0 = np.zeros_like(x0)

    # deformation
    ux = u_array[::5].reshape(x0.shape)
    uy = u_array[1::5].reshape(x0.shape)
    uz = u_array[2::5].reshape(x0.shape)

    # deformed 
    xd = x0 + ux
    yd = y0 + uy
    zd = z0 + uz

    xd = xd.flatten()
    yd = yd.flatten()
    zd = zd.flatten()


    # element activity flag
    z_onoff = np.zeros(N_elem**2)
    for i in xrange(N_elem**2):
        z_onoff[i] = z_array[4*i] + z_array[4*i+1] \
        + z_array[4*i+2] + z_array[4*i+3]

    linewidth = 3
    alpha = 0.5

    for i in xrange(N_elem**2):
        if z_onoff[i] > 0:
            ind_loc = elem[i]

            line1_x = [xd[ind_loc[0]], xd[ind_loc[1]]]
            line1_y = [yd[ind_loc[0]], yd[ind_loc[1]]]
            line1_z = [zd[ind_loc[0]], zd[ind_loc[1]]]
            ax.plot3D(line1_x, line1_y, line1_z, color, linewidth=linewidth, alpha=alpha)

            line2_x = [xd[ind_loc[1]], xd[ind_loc[2]]]
            line2_y = [yd[ind_loc[1]], yd[ind_loc[2]]]
            line2_z = [zd[ind_loc[1]], zd[ind_loc[2]]]
            ax.plot3D(line2_x, line2_y, line2_z, color, linewidth=linewidth, alpha=alpha)

            line3_x = [xd[ind_loc[2]], xd[ind_loc[5]]]
            line3_y = [yd[ind_loc[2]], yd[ind_loc[5]]]
            line3_z = [zd[ind_loc[2]], zd[ind_loc[5]]]
            ax.plot3D(line3_x, line3_y, line3_z, color, linewidth=linewidth, alpha=alpha)

            line4_x = [xd[ind_loc[5]], xd[ind_loc[8]]]
            line4_y = [yd[ind_loc[5]], yd[ind_loc[8]]]
            line4_z = [zd[ind_loc[5]], zd[ind_loc[8]]]
            ax.plot3D(line4_x, line4_y, line4_z, color, linewidth=linewidth, alpha=alpha)

            line5_x = [xd[ind_loc[8]], xd[ind_loc[7]]]
            line5_y = [yd[ind_loc[8]], yd[ind_loc[7]]]
            line5_z = [zd[ind_loc[8]], zd[ind_loc[7]]]
            ax.plot3D(line5_x, line5_y, line5_z, color, linewidth=linewidth, alpha=alpha)

            line6_x = [xd[ind_loc[7]], xd[ind_loc[6]]]
            line6_y = [yd[ind_loc[7]], yd[ind_loc[6]]]
            line6_z = [zd[ind_loc[7]], zd[ind_loc[6]]]
            ax.plot3D(line6_x, line6_y, line6_z, color, linewidth=linewidth, alpha=alpha)

            line7_x = [xd[ind_loc[6]], xd[ind_loc[3]]]
            line7_y = [yd[ind_loc[6]], yd[ind_loc[3]]]
            line7_z = [zd[ind_loc[6]], zd[ind_loc[3]]]
            ax.plot3D(line7_x, line7_y, line7_z, color, linewidth=linewidth, alpha=alpha)

            line8_x = [xd[ind_loc[3]], xd[ind_loc[0]]]
            line8_y = [yd[ind_loc[3]], yd[ind_loc[0]]]
            line8_z = [zd[ind_loc[3]], zd[ind_loc[0]]]
            ax.plot3D(line8_x, line8_y, line8_z, color, linewidth=linewidth, alpha=alpha)

            a = Arrow3D([xd[-N_elem*2-1],xd[-N_elem*2-1]],
            [yd[-N_elem*2-1],yd[-N_elem*2-1]],
            [zd[-N_elem*2-1],zd[-N_elem*2-1]+0.3], 
            mutation_scale=20, lw=3, arrowstyle="-|>", color="k")
            ax.add_artist(a)

    return ax

# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# ax.set_aspect('equal')

# if flag_le:
#     ax = plotMesh(ax, var_name_le_list, var_value_le_list, color=my_green)
# if flag_se:
#     ax = plotMesh(ax, var_name_se_list, var_value_se_list, color=my_blue)
# if flag_DCC_dense:
#     ax = plotMesh(ax, var_name_DCC_dense_list, var_value_DCC_dense_list, color=my_red)
# if flag_DCC_sparse:
#     ax = plotMesh(ax, var_name_DCC_sparse_list, var_value_DCC_sparse_list, color=my_brown)


# ax.set_xlim(0, 1)
# ax.set_ylim(0, 1)
# ax.set_zlim(-0.5, 0.5)




# plt.show()
