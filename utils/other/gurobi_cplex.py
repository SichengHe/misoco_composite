

import sys 


filename = sys.argv[1] + ".mps"
solver = int(sys.argv[2])
threads = 4


if solver == 0 : # Xpress
        import xpress as xp 
	p = xp.problem ()
	p.read (filename)
	p.setControl ('mipthreads', threads)
	p.setControl ('threads', threads)
	
	
	
	

	p.solve ()

elif solver == 1: # Cplex 
	import cplex
	from cplex.exceptions import CplexError

	p = cplex.Cplex(filename)
	p.parameters.threads.set(threads)
	
	# p.parameters.lpmethod.set(2)
	
	# #1: primal simplex 2: dual simplex 4:IPM
	# p.parameters.mip.strategy.startalgorithm.set(1)
	
	# p.parameters.tune.display.set(3)
	# p.parameters.barrier.crossover.set(2)
	p.solve()
	
	
elif solver == 2:  # Gurobi
	from gurobipy import * 

	p = read(filename)
	p.Params.Threads = 4
	p.optimize()  



