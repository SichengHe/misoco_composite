import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
import numpy as np

my_blue = '#4C72B0'
my_red = '#C54E52'
my_green = '#56A968' 
my_brown = '#b4943e'
			
def getIncumbentBestBd(file):

    time_list = []
    BestBd_list = []
    Incumbent_list = []

    with open(file, "r") as f:
        for line in f:
            content = line.split()

            # time
            time = content[-1]
            time = float(time[:-1])
            time_list.append(time)

            # best bd
            BestBd = float(content[-4])
            BestBd_list.append(BestBd)

            # incumbent
            if not content[-5] == '-':

                Incumbent = float(content[-5])
                Incumbent_list.append(Incumbent)

    # close the gap
    last_time = time_list[-1]
    time_list.append(last_time)

    last_incumbent = Incumbent_list[-1]
    Incumbent_list.append(last_incumbent)
    BestBd_list.append(last_incumbent)

    return time_list, BestBd_list, Incumbent_list

def plotHistory(time_list, BestBd_list, Incumbent_list, color=my_blue, label='test1'):

    len_incumbent = len(Incumbent_list)
    time_incumbent = time_list[-len_incumbent:]

    plt.plot(time_incumbent, Incumbent_list, linewidth=3, color=color, label=(label + ' ' + 'incumbent' ) )
    plt.plot(time_list, BestBd_list, linewidth=6, color=color, label=label + '' + 'best bound')

file = 'le.txt'
color = my_blue
label = 'large ellipsoid'
time_list, BestBd_list, Incumbent_list = getIncumbentBestBd(file)
plotHistory(time_list, BestBd_list, Incumbent_list, color=color, label=label)

file = 'se.txt'
color = my_red
label = 'small ellipsoid'
time_list, BestBd_list, Incumbent_list = getIncumbentBestBd(file)
plotHistory(time_list, BestBd_list, Incumbent_list, color=color, label=label)

# file = 'sparse.txt'
# color = my_green
# label = 'sparse DCC'
# time_list, BestBd_list, Incumbent_list = getIncumbentBestBd(file)
# plotHistory(time_list, BestBd_list, Incumbent_list, color=color, label=label)

# file = 'dense.txt'
# color = my_brown
# label = 'dense DCC'
# time_list, BestBd_list, Incumbent_list = getIncumbentBestBd(file)
# plotHistory(time_list, BestBd_list, Incumbent_list, color=color, label=label)

plt.rc('legend',**{'fontsize':20})

plt.legend()

pylab.xlabel('Time (sec)', fontsize=20)
pylab.ylabel('Weight', fontsize=20)

plt.tick_params(axis='both', labelsize=20)

plt.show()