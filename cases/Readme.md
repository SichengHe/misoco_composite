## Run Gurobi
We have listed all the command line inputs corresponding with the test cases listed in our paper for using Gurobi here.

### Mass minimization cases

* $3\times 3$ discrete ply-angle only
```
python run_realistic.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --flag_le True --thread 6 --FlagWeight True --jobName mass_3x3_plyOnly
python run_realistic.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --flag_se True --thread 6 --FlagWeight True --jobName mass_3x3_plyOnly
python run_realistic.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --flag_DCC_sparse True --thread 6 --FlagWeight True --jobName mass_3x3_plyOnly
python run_realistic.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --flag_DCC_dense True --thread 6 --FlagWeight True --jobName mass_3x3_plyOnly
```

* $3\times 3$ discrete ply-angle and thickness
```
python run_realistic.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --flag_le True --thread 6 --FlagWeight True --flagPlyAngleOnly False --jobName mass_3x3_plyThick
python run_realistic.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --flag_se True --thread 6 --FlagWeight True --flagPlyAngleOnly False --jobName mass_3x3_plyThick
python run_realistic.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --flag_DCC_sparse True --thread 6 --FlagWeight True --flagPlyAngleOnly False --jobName mass_3x3_plyThick
python run_realistic.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --flag_DCC_dense True --thread 6 --FlagWeight True --flagPlyAngleOnly False --jobName mass_3x3_plyThick
```

* $4\times 4$ discrete ply-angle only
```
python run_realistic.py --N_elem 4 --N_patch 4 --alpha_m 1e-6 --alpha_s 1e-2 --flag_le True --thread 6 --FlagWeight True --jobName mass_4x4_plyOnly
python run_realistic.py --N_elem 4 --N_patch 4 --alpha_m 1e-6 --alpha_s 1e-2 --flag_se True --thread 6 --FlagWeight True --jobName mass_4x4_plyOnly
python run_realistic.py --N_elem 4 --N_patch 4 --alpha_m 1e-6 --alpha_s 1e-2 --flag_DCC_sparse True --thread 6 --FlagWeight True --jobName mass_4x4_plyOnly
python run_realistic.py --N_elem 4 --N_patch 4 --alpha_m 1e-6 --alpha_s 1e-2 --flag_DCC_dense True --thread 6 --FlagWeight True --jobName mass_4x4_plyOnly
```

* $4\times 4$ discrete ply-angle and thickness
```
python run_realistic.py --N_elem 4 --N_patch 4 --alpha_m 1e-6 --alpha_s 1e-2 --flag_le True --thread 6 --FlagWeight True --flagPlyAngleOnly False --jobName mass_4x4_plyThick
python run_realistic.py --N_elem 4 --N_patch 4 --alpha_m 1e-6 --alpha_s 1e-2 --flag_se True --thread 6 --FlagWeight True --flagPlyAngleOnly False --jobName mass_4x4_plyThick
python run_realistic.py --N_elem 4 --N_patch 4 --alpha_m 1e-6 --alpha_s 1e-2 --flag_DCC_sparse True --thread 6 --FlagWeight True --flagPlyAngleOnly False --jobName mass_4x4_plyThick
python run_realistic.py --N_elem 4 --N_patch 4 --alpha_m 1e-6 --alpha_s 1e-2 --flag_DCC_dense True --thread 6 --FlagWeight True --flagPlyAngleOnly False --jobName mass_4x4_plyThick
```

### Compliance minimization cases
The mass bound is chosen by multiplying a factor to the corresponding mass minimization problem results.
They are $5.4$ for $3\times 3$ discrete ply-angle problem; $94.0$ for $3\times 3$ discrete ply-angle and thickness problem; $8.0$ for $4\times 4$ discrete ply-angle problem; $86.0$ for $4\times 4$ discrete ply-angle and thickness problem.

* $3\times 3$ discrete ply-angle only
```
python run_realistic.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --flag_le True --thread 6 --FlagWeight False --totalWeight 5.4 --jobName comp_3x3_plyOnly
python run_realistic.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --flag_se True --thread 6 --FlagWeight False --totalWeight 5.4 --jobName comp_3x3_plyOnly
python run_realistic.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --flag_DCC_sparse True --thread 6 --FlagWeight False --totalWeight 5.4 --jobName comp_3x3_plyOnly
python run_realistic.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --flag_DCC_dense True --thread 6 --FlagWeight False --totalWeight 5.4 --jobName comp_3x3_plyOnly
```

* $3\times 3$ discrete ply-angle and thickness
```
python run_realistic.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --flag_le True --thread 6 --FlagWeight False --totalWeight 94.0 --flagPlyAngleOnly False --jobName comp_3x3_plyThick
python run_realistic.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --flag_se True --thread 6 --FlagWeight False --totalWeight 94.0 --flagPlyAngleOnly False --jobName comp_3x3_plyThick
python run_realistic.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --flag_DCC_sparse True --thread 6 --FlagWeight False --totalWeight 94.0 --flagPlyAngleOnly False --jobName comp_3x3_plyThick
python run_realistic.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --flag_DCC_dense True --thread 6 --FlagWeight False --totalWeight 94.0 --flagPlyAngleOnly False --jobName comp_3x3_plyThick
```

* $4\times 4$ discrete ply-angle only
```
python run_realistic.py --N_elem 4 --N_patch 4 --alpha_m 1e-6 --alpha_s 1e-2 --flag_le True --thread 6 --FlagWeight False --totalWeight 8.0 --jobName comp_4x4_plyOnly
python run_realistic.py --N_elem 4 --N_patch 4 --alpha_m 1e-6 --alpha_s 1e-2 --flag_le True --thread 6 --FlagWeight False --totalWeight 8.0 --jobName comp_4x4_plyOnly
python run_realistic.py --N_elem 4 --N_patch 4 --alpha_m 1e-6 --alpha_s 1e-2 --flag_le True --thread 6 --FlagWeight False --totalWeight 8.0 --jobName comp_4x4_plyOnly
python run_realistic.py --N_elem 4 --N_patch 4 --alpha_m 1e-6 --alpha_s 1e-2 --flag_le True --thread 6 --FlagWeight False --totalWeight 8.0 --jobName comp_4x4_plyOnly
```

* $4\times 4$ discrete ply-angle and thickness
```
python run_realistic.py --N_elem 4 --N_patch 4 --alpha_m 1e-6 --alpha_s 1e-2 --flag_le True --thread 6 --FlagWeight False --totalWeight 86.0 --flagPlyAngleOnly False --jobName comp_4x4_plyThick
python run_realistic.py --N_elem 4 --N_patch 4 --alpha_m 1e-6 --alpha_s 1e-2 --flag_le True --thread 6 --FlagWeight False --totalWeight 86.0 --flagPlyAngleOnly False --jobName comp_4x4_plyThick
python run_realistic.py --N_elem 4 --N_patch 4 --alpha_m 1e-6 --alpha_s 1e-2 --flag_le True --thread 6 --FlagWeight False --totalWeight 86.0 --flagPlyAngleOnly False --jobName comp_4x4_plyThick
python run_realistic.py --N_elem 4 --N_patch 4 --alpha_m 1e-6 --alpha_s 1e-2 --flag_le True --thread 6 --FlagWeight False --totalWeight 86.0 --flagPlyAngleOnly False --jobName comp_4x4_plyThick
```

## Run CPLEX
We will use Gurobi to generate the case .mps file and then send the file to CPLEX.
Simply add one more argument will be fine: `--flagCPLEXGenerator = True`.
e.g.
```
python run_realistic.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --flag_le True --thread 6 --FlagWeight True --jobName mass_3x3_plyOnly --flagCPLEXGenerator = True
```


## Run BARON
We solve the problem using BARON.

### MINLO
#### Mass minimization


* $3\times 3$ discrete ply-angle only
```
python run_baron.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --FlagWeight True --flag_MINLO True --thread 6
```

#### Compliance minimization
* $3\times 3$ discrete ply-angle only
```
python run_baron.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --FlagWeight False --totalWeight 5.4 --flag_MINLO True --thread 6
```

### MISOCO-se
#### Mass minimization


* $3\times 3$ discrete ply-angle only
```
python run_baron.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --FlagWeight True --thread 6 --flag_se True
python run_baron.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --FlagWeight True --thread 6 --flag_le True
python run_baron.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --FlagWeight True --thread 6 --flag_DCC_sparse True
python run_baron.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --FlagWeight True --thread 6 --flag_DCC_dense True
```

#### Compliance minimization
* $3\times 3$ discrete ply-angle only
```
python run_baron.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --FlagWeight False --totalWeight 5.4 --thread 6 --flag_se True
python run_baron.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --FlagWeight False --totalWeight 5.4 --thread 6 --flag_le True
python run_baron.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --FlagWeight False --totalWeight 5.4 --thread 6 --flag_DCC_sparse True
python run_baron.py --N_elem 3 --N_patch 3 --alpha_m 1e-6 --alpha_s 1e-2 --FlagWeight False --totalWeight 5.4 --thread 6 --flag_DCC_dense True
```
