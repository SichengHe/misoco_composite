import os
import os.path
import sys

cwd = os.getcwd()
cwd_parent = os.path.abspath(os.path.join(cwd, os.pardir))

sys.path.append(cwd_parent) 
sys.path.append(cwd_parent + '/utils/composite') # composte utils
sys.path.append(cwd_parent + '/formulations') # formulations

import topo as topo
from plate_layerwise import *
from gen_neighborSet import gen_neighborSet
from gen_conn import gen_conn
from gen_group import gen_group
import numpy as np
import copy as copy
import argparse
from opt_baron import opt_baron
from opt_baron_MISOCO import opt_baron_MISOCO



def str2boolean(flag):
    if flag.lower() in ('true', '1'):
        return True
    elif flag.lower() in ('false', '0'):
        return False
    else:
        print("error: boolean value not set correctly")
        exit()


parser = argparse.ArgumentParser()

parser.add_argument("--N_elem", help="number of element per side",type=int)
parser.add_argument("--N_patch", help="numer of patch per side",type=int)
parser.add_argument("--alpha_m", help="scaling factor alpha_m",type=float)
parser.add_argument("--alpha_s", help="scaling factor alpha_s",type=float)
parser.add_argument("--thread", help="number of thread to be used", type=int, default=6)
parser.add_argument("--FlagWeight", help="whether it is a weight optimization problem, or compliance optimization problem", type=str, default="True")
parser.add_argument("--totalWeight", help="maximum total weight for compliance optimization problem",type=float)
parser.add_argument("--flag_MINLO", help="whether use general MINLO formulation", type=str, default="False")
parser.add_argument("--flag_se", help="whether use general MISOCO se formulation", type=str, default="False")
parser.add_argument("--flag_le", help="whether use general MISOCO le formulation", type=str, default="False")
parser.add_argument("--flag_DCC_sparse", help="whether use general MISOCO DCC sparse formulation", type=str, default="False")
parser.add_argument("--flag_DCC_dense", help="whether use general MISOCO DCC dense formulation", type=str, default="False")


args = parser.parse_args()
N_elem = args.N_elem
N_patch = args.N_patch
alpha_m = args.alpha_m
alpha_s = args.alpha_s
thread = args.thread
FlagWeight = str2boolean(args.FlagWeight)
totalWeight = args.totalWeight
flag_MINLO = str2boolean(args.flag_MINLO)
flag_se = str2boolean(args.flag_se)
flag_le = str2boolean(args.flag_le)
flag_DCC_sparse = str2boolean(args.flag_DCC_sparse)
flag_DCC_dense = str2boolean(args.flag_DCC_dense)

print("============================================================")
print("parameters || thread | N_elem | N_patch | alpha_m | alpha_s |")
print("------------------------------------------------------------")
print("           ||  ", thread, "   |   ",  N_elem, "  |  ",  N_patch, "    |", alpha_m, "  |  ", alpha_s, " |")
print("============================================================")

# ===========================================================
#    Model preprocess                                       
# ===========================================================

# length and thickness (per element)
l = 1.0/N_elem
h = 1e-1

#  generate the patch (list of element index with the same design (angle)) 
patchedElem_list = gen_group(N_elem, N_patch)

# generate elem, node, BC files
dir = os.getcwd() 
Inputdir = dir + '/Input'
topo.topology_generator_sqr(N_elem, l, Inputdir)



# upper lower bound per node for 5 d.o.f
# lb = [-0.2]*5
# ub = [0.2]*5
lb = [-0.0, -0.0, -0.3, -0.3, -0.3] # HACK: assume load on y direction --  no inplane motion
ub = [0.0, 0.0, 0.3, 0.3, 0.3] # HACK: same as above

# node 
node = np.loadtxt(Inputdir + '/' + "node.txt")
node_N = len(node)

lb_list = []
ub_list = []
for i in xrange(node_N):
    lb_list.append(lb)
    ub_list.append(ub)

# elem2node
elem = np.loadtxt(Inputdir + '/' + "elem.txt")
elem = elem.astype(int)
elem_N = len(elem)

# Boundary condition
BC = np.loadtxt(Inputdir + '/' + "BC.txt")
BC = BC.astype(int)

# external load
f = np.zeros(len(node)*5)
f[-(2*N_elem+1-1)*5-3] = 6.0*1e5*0.7 # HACK: the load is found approapriate from numerical test
# f[-(2*N_elem+1-1)*5-3] = 6.0 # HACK: the load is found approapriate from numerical test



# material
E1   = 146860.0*1e6
E2   = 10620.0*1e6
G12  = 5450.0*1e6
G23  = 3990.0*1e6
nu12 = 0.33

Evec = [E1, E2, G12, G23, nu12]

Xt = 1035.0*1e6
Xc = 689.0*1e6
Yt = 41.0*1e6
Yc = 117.0*1e6
S = 69.0*1e6


# candidate set and candidate properties
theta_list = [0, np.pi/4, np.pi/2, np.pi/4*3.0]

K_option_list = []

Pj_option_list = []
qj_option_list = []

Qj_option_list = []

for i in xrange(len(theta_list)):
    theta = theta_list[i]
    layer = layerElement(Evec, theta, h, [l,l])
    layer.setTsaiWuStressParam(Xt, Xc, Yt, Yc, S)

    K_loc = layer.stiffnessMat()

    K_option_list.append(K_loc)

    layer.setTsaiWuStressBasic()

    Pj_option_list.append(layer.Pj)
    qj_option_list.append(layer.qj)


    Qj_loc = layer.QmatFun()

    Qj_option_list.append(Qj_loc)



B = layer.center_inplane_strain()

B_option_list = []
for i in xrange(len(theta_list)):
    B_option_list.append(B)



# neighbor set and connectivity set
# used to enforce no floating element constraint and manufacturing constraint
neighborSet = gen_neighborSet(N_elem)
conn = gen_conn(N_elem)


def scaling_GK(alpha_m, alpha_s, \
K_option_list, Qj_option_list, f, lb_list, ub_list):

    # implement Graeme Kennedy's two parameter scaling method
    # A full-space barrier method for stress-constrained discrete material design optimization

    K_option_list_scaled = []
    Qj_option_list_scaled = []
    f_scaled = copy.deepcopy(f)
    lb_list_scaled = []
    ub_list_scaled = []

    for i in xrange(len(K_option_list)):
        K_option_list_scaled.append(K_option_list[i]*alpha_m)

    for i in xrange(len(Qj_option_list)):
        Qj_option_list_scaled.append( Qj_option_list[i]*alpha_s)
    
    f_scaled *= alpha_m/alpha_s

    for i in xrange(len(lb_list)):

        lb_list_loc = []
        ub_list_loc = []

        for j in xrange(5):

            lb_list_loc.append(lb_list[i][j] / alpha_s)
            ub_list_loc.append(ub_list[i][j] / alpha_s)

        lb_list_scaled.append(lb_list_loc)
        ub_list_scaled.append(ub_list_loc)

    return K_option_list_scaled, Qj_option_list_scaled, f_scaled, lb_list_scaled, ub_list_scaled


def getCheckerboardSet(N):

    checkerboardSet = []

    for i in xrange(N-1):
        for j in xrange(N-1):

            # ind2 | ind4
            # -----------
            # ind1 | ind3

            ind1 = i*N + j
            ind2 = i*N + j + 1
            ind3 = (i + 1)*N + j
            ind4 = (i + 1)*N + j + 1

            checkerboardSet.append([ind1, ind2, ind3, ind4])

    return checkerboardSet


checkerboardSet = getCheckerboardSet(N_elem)

K_option_list, Qj_option_list, f, lb_list, ub_list = \
scaling_GK(alpha_m, alpha_s, K_option_list, Qj_option_list, f, lb_list, ub_list)


if flag_se:
    opt_baron_MISOCO(K_option_list,\
    Pj_option_list, qj_option_list, Qj_option_list,\
    B_option_list, \
    node, elem, BC, f,\
    lb_list, ub_list, \
    conn = conn, patchedElem_list = patchedElem_list, Threads = thread, checkerboardSet = checkerboardSet,\
    FlagWeight = FlagWeight, totalWeight = totalWeight, formulation = "se")

elif flag_le:
    opt_baron_MISOCO(K_option_list,\
    Pj_option_list, qj_option_list, Qj_option_list,\
    B_option_list, \
    node, elem, BC, f,\
    lb_list, ub_list, \
    conn = conn, patchedElem_list = patchedElem_list, Threads = thread, checkerboardSet = checkerboardSet,\
    FlagWeight = FlagWeight, totalWeight = totalWeight, formulation = "le")

elif flag_DCC_sparse:
    opt_baron_MISOCO(K_option_list,\
    Pj_option_list, qj_option_list, Qj_option_list,\
    B_option_list, \
    node, elem, BC, f,\
    lb_list, ub_list, \
    conn = conn, patchedElem_list = patchedElem_list, Threads = thread, checkerboardSet = checkerboardSet,\
    FlagWeight = FlagWeight, totalWeight = totalWeight, formulation = "DCC_sparse")

elif flag_DCC_dense:
    opt_baron_MISOCO(K_option_list,\
    Pj_option_list, qj_option_list, Qj_option_list,\
    B_option_list, \
    node, elem, BC, f,\
    lb_list, ub_list, \
    conn = conn, patchedElem_list = patchedElem_list, Threads = thread, checkerboardSet = checkerboardSet,\
    FlagWeight = FlagWeight, totalWeight = totalWeight, formulation = "DCC_dense")    

elif flag_MINLO:
    opt_baron(K_option_list,\
    Pj_option_list, qj_option_list, Qj_option_list,\
    B_option_list, \
    node, elem, BC, f,\
    lb_list, ub_list, \
    conn = conn, patchedElem_list = patchedElem_list, Threads = thread, checkerboardSet = checkerboardSet,\
    FlagWeight = FlagWeight, totalWeight = totalWeight)



