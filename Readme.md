# Purpose
This is the repo for test cases used in the paper [Mixed-integer Second-order Cone Optimization for Composite Discrete Ply-angle and Thickness Topology Optimization Problems](https://engineering.lehigh.edu/sites/engineering.lehigh.edu/files/_DEPARTMENTS/ise/pdf/tech-papers/19T_014a.pdf).
The purpose of the repo is to enhance the reproductivity of this research.
![alt_text](4x4_weight_da.png)


If you find the repo helpful, please consider citing us (refer to the reference)!

# Content
The repo has the following contents:

* Mass and compliance optimization cases with discrete ply-angle only or also with thickness set up in [Gurobi](https://www.gurobi.com/) programmed in Python.
* The cases could be used to generated `.mps` files intended to be used for [CPLEX](https://www.ibm.com/analytics/cplex-optimizer).
* Optimization formulations and cases set up in [Baron](https://minlp.com/baron) programmed in Python.

# Organization
The users are intended to run cases in the `/case` directory using the commands listed in `/case/Readme.md`.
It will use different mathematical programming formulations in `/formulations` directory.
The underlying composite model is implemented in `/utils/composite` which is used to generate many matrices used in formulations.
Finally, there are also some plotting tools and etc in `/utils/other`.

# References

- Sicheng He, Joaquim R. R. A. Martins, Mohammad Shahabsafa, Ali Mohammad-Nezhad, Weiming Lei, Luis Zuluaga, and Tamas Terlaky, 
  [Mixed-integer Second-order Cone Optimization for Composite Discrete Ply-angle and Thickness Topology Optimization Problems](https://engineering.lehigh.edu/sites/engineering.lehigh.edu/files/_DEPARTMENTS/ise/pdf/tech-papers/19T_014a.pdf),
  submitted to $\textit{Optimization and Engineering}$.


# TODOs

* Add the missing file for converting MISOCO formulation `.mps` file to [MOSEK](https://www.mosek.com/). 
It requires using some trick to convert the conic constraint.
We had that...
* Ideally, we want to merge several formulations into one files and use flag to control the flow instead of having duplicated codes.
* BARON optimizer uses the underlying CPLEX linear algebra library to speed things up. 
We hard-coded the directory, which ideally shall be independent from platform. 
The user has the responsibility to link the library correctly by themselves inside the file `/formulations/opt_baron.py`.
  