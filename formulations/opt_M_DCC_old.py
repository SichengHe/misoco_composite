import os
cwd = os.getcwd()

import sys
sys.path.append(cwd + '/utils') 

import numpy as np
import time
from gurobipy import *
from plate_layerwise import *
import copy



def opt_M_DCC_old(K_option_list, \
    Pj_option_list, qj_option_list, Qj_option_list,\
    B_option_list, \
    node, elem, BC, f,\
    lb, ub,\
    conn = None, neighborSet=None, colSet=None, patchedElem_list=None, Threads=None,\
    checkerboardSet=None, FlagWeight=True, totalWeight=0, h_option_list=[],\
    jobName = None, flagCPLEXGenerator = False):

    dof_per_node = 5
    choice_N = len(K_option_list)
    node_N = len(node)
    elem_N = len(elem)


    # ================================================================
    #  Optimization                         
    # ================================================================

    # model
    m = Model("weight minimization")


    # ================================================================
    # add var 
    # ================================================================

    # choice variables
    z = np.zeros((elem_N,choice_N)).tolist()

    for i in xrange(elem_N):
        for j in xrange(choice_N):

            z[i][j] = m.addVar(vtype=GRB.BINARY,name='z%i%i' %(i,j))
            # z[i][j] = m.addVar(vtype=GRB.CONTINUOUS, lb=0.0, ub=1.0, name='z%i%i' %(i,j))


    # displacement variables
    u = np.zeros((node_N,dof_per_node)).tolist()

    for i in xrange(node_N):
        for j in xrange(dof_per_node):
            u[i][j] = m.addVar(vtype=GRB.CONTINUOUS,lb=lb[i][j],ub=ub[i][j],name='u%i%i' %(i,j))

    # pseudo displacement variables
    psi = np.zeros((elem_N, choice_N, dof_per_node*9)).tolist()

    for i in xrange(elem_N):
        for j in xrange(choice_N):
            for k in xrange(dof_per_node*9):

                psi[i][j][k] = m.addVar(vtype=GRB.CONTINUOUS,lb=-GRB.INFINITY,name='psi%i,%i,%i' %(i,j,k))

    # pseudo strain variables (at the center of each element)
    # ordered as (e_xx,e_yy,e_xy) (Just the inplane components)
    epsilon = np.zeros((elem_N,choice_N,3)).tolist()

    for i in xrange(elem_N):
        for j in xrange(choice_N):
            for k in xrange(3):

                epsilon[i][j][k] = m.addVar(vtype=GRB.CONTINUOUS,lb=-GRB.INFINITY,name='epsilon%i%i%i' %(i,j,k))

    # v variables (at the center of each element) v=lambda P (epsilon, z) -- intermediate variables introduced for SOC constraints

    v = np.zeros((elem_N, choice_N, 4)).tolist()
    for i in xrange(elem_N):
        for j in xrange(choice_N):
            for k in xrange(4):

                if k==0:

                    v[i][j][k] = m.addVar(vtype=GRB.CONTINUOUS,lb=0.0,name='v%i%i%i' %(i,j,k))

                else:

                    v[i][j][k] = m.addVar(vtype=GRB.CONTINUOUS,lb=-GRB.INFINITY,name='v%i%i%i' %(i,j,k))





    m.update()

    # ================================================================
    # warm start 
    # ================================================================
    for i in xrange(elem_N):

        if 0: # full guess
            z[i][0].start = 0.0
            z[i][1].start = 0.0
            z[i][2].start = 0.0
            z[i][3].start = 1.0

    # ================================================================
    # add obj 
    # ================================================================
    if FlagWeight:
        # weight minimization 

        weight = 0.0

        if (len(h_option_list) == 0):
            #w/o discrete thickness
            for i in xrange(elem_N):
                for j in xrange(choice_N):

                    weight += z[i][j]

            m.setObjective(weight, GRB.MINIMIZE)

        else:
            # discrete thickness
            for i in xrange(elem_N):
                for j in xrange(choice_N):

                    weight += z[i][j]*h_option_list[j]

            m.setObjective(weight, GRB.MINIMIZE)
    else:
        # compliance
        compliance = 0.0
        for i in xrange(node_N):
            if(i not in BC):
                for j in xrange(dof_per_node):

                    index_loc = i*dof_per_node + j
                    compliance += f[index_loc]*u[i][j]
        
        m.setObjective(compliance, GRB.MINIMIZE)
        
    # ================================================================
    # add constraints 
    # ================================================================
    # maximum weight
    if (not FlagWeight):
        # weight constraint
        if (len(h_option_list) == 0):           
            glo_sum_z = 0.0
            for i in xrange(elem_N):
                for j in xrange(choice_N):
                    glo_sum_z += z[i][j]

            m.addConstr(glo_sum_z<=totalWeight)
        else:
            glo_sum_z = 0.0
            for i in xrange(elem_N):
                for j in xrange(choice_N):
                    glo_sum_z += z[i][j]*h_option_list[j]

            m.addConstr(glo_sum_z<=totalWeight)


    # equlibrium constraints
    f_sum = np.zeros(node_N*dof_per_node).tolist()
    for i in xrange(elem_N):

        element2Node_loc = elem[i]

        f_loc = np.zeros(9*dof_per_node).tolist()
        for j in xrange(choice_N):

            Kij = K_option_list[j]

            for ii in xrange(9*dof_per_node):
                for jj in xrange(9*dof_per_node):

                    f_loc[ii] += Kij[ii,jj]*psi[i][j][jj]

        for ii in xrange(9): 

            glo_index = element2Node_loc[ii]

            for jj in xrange(dof_per_node):

                f_sum[glo_index*dof_per_node+jj] += f_loc[ii*dof_per_node+jj]

    for i in xrange(node_N):
        if(i not in BC):
            for j in xrange(dof_per_node):

                index_loc = i*dof_per_node + j
                m.addConstr(f_sum[index_loc] == f[index_loc])


    # fixed BC constraints 
    for ind_BC_loc in BC:
        for j in xrange(dof_per_node):

            m.addConstr(u[ind_BC_loc][j] == 0.0)

    
    # big-M method
    for i in xrange(elem_N):

        elem_loc = elem[i]

        for j in xrange(choice_N):
            for ii in xrange(9):

                glo_index = elem_loc[ii]
                u_loc = u[glo_index][:]

                psi_loc = psi[i][j][ii*dof_per_node:(ii + 1)*dof_per_node]

                for jj in xrange(dof_per_node):

                    m.addConstr(psi_loc[jj]<=ub[glo_index][jj]*z[i][j])
                    m.addConstr(psi_loc[jj]>=lb[glo_index][jj]*z[i][j])
                    m.addConstr(psi_loc[jj]<=u_loc[jj]-lb[glo_index][jj]*(1-z[i][j]))
                    m.addConstr(psi_loc[jj]>=u_loc[jj]-ub[glo_index][jj]*(1-z[i][j]))


    # displacement and strain relationship constraints
    for i in xrange(elem_N):
        for j in xrange(choice_N): 

            B_loc = B_option_list[j]

            for ii in xrange(3):

                epsilon_loc = 0.0

                for jj in xrange(9*dof_per_node):

                    epsilon_loc += B_loc[ii, jj]*psi[i][j][jj]

                m.addConstr(epsilon_loc == epsilon[i][j][ii])




    # Tsai-Wu stress constraints (with DCC)
    # Pj_option_list, qj_option_list, Qj_option_list
    QPQ_option_list = []

    for i in xrange(choice_N):

        Pj_loc = Pj_option_list[i]
        qj_loc = qj_option_list[i]
        Qj_loc = Qj_option_list[i]

        Pqqr = np.zeros((4, 4))
        Pqqr[:3, :3] += Pj_loc
        Pqqr[:3, 3:] += qj_loc
        Pqqr[3:, :3] += np.transpose(qj_loc)
        Pqqr[3, 3] = -1.0

        Q1 = np.zeros((4, 4))
        Q1[:3, :3] += Qj_loc
        Q1[3, 3] = 1.0

        QPQ_loc = np.transpose(Q1).dot(Pqqr).dot(Q1)

        QPQ_option_list.append(QPQ_loc)


    for i in xrange(elem_N):
        for j in xrange(choice_N):

            QPQ_loc = QPQ_option_list[j]

            [eig_val, P] = np.linalg.eig(QPQ_loc)
            eig_val = eig_val.tolist()

            ind_neg = eig_val.index(min(eig_val))

            if not ind_neg == 0:

                P0 = copy.deepcopy(P[:, 0])
                Pneg = copy.deepcopy(P[:, ind_neg])

                eig0 = eig_val[0]
                eigneg = eig_val[ind_neg]

                # switch them!

                P[:, 0] = Pneg
                P[:, ind_neg] = P0

                eig_val[0] = eigneg
                eig_val[ind_neg] = eig0

            # make sure the right branch of cone picked
            if P[-1, 0] <= 0.0:

                P[:, 0] = -P[:, 0]


            # get the square root of absolute value of the eigenvalues
            abs_eig_val = np.diag(np.abs(copy.deepcopy(eig_val)))

            root_abs_eig_val = np.zeros((4, 4))
            for ii in xrange(4):
                root_abs_eig_val[ii, ii] = np.sqrt(abs_eig_val[ii, ii])

            lambda_P = root_abs_eig_val.dot(np.transpose(P))




            # constraibtns for intermediate vars -- v:
            for ii in xrange(4):

                sum_prod = 0.0

                for jj in xrange(3):
                    sum_prod += lambda_P[ii, jj]*epsilon[i][j][jj]

                sum_prod += lambda_P[ii, 3]*z[i][j]

                m.addConstr(sum_prod == v[i][j][ii])


            # add the cone constraints
            sum_prod = 0.0
            for ii in xrange(3):

                sum_prod += v[i][j][ii+1]*v[i][j][ii+1]

            m.addQConstr(sum_prod - v[i][j][0]*v[i][j][0] <= 0.0, 'Tsai-Wu%i%i' %(i,j))


    # topology constraint
    for i in xrange(elem_N):

        sum_z = 0.0

        for j in xrange(choice_N):
            sum_z += z[i][j]

        m.addConstr(sum_z<=1.0)
    
    # checkerboard constraint
    if (not checkerboardSet==None):
        for i in xrange(len(checkerboardSet)):

            # ind2 | ind4
            # -----------
            # ind1 | ind3

            ind1, ind2, ind3, ind4 = checkerboardSet[i][:]

            z1 = 0.0
            z2 = 0.0
            z3 = 0.0
            z4 = 0.0
            for j in xrange(choice_N):
                z1 += z[ind1][j]
                z2 += z[ind2][j]
                z3 += z[ind3][j]
                z4 += z[ind4][j]

            m.addConstr((z1 + z4) - (z2 + z3) <=  1.0)
            m.addConstr((z1 + z4) - (z2 + z3) >= -1.0)


    # manufacturing constraint
    if (not conn==None):
        for i in xrange(len(conn)):

            ind1 = conn[i][0]
            ind2 = conn[i][1]

            sum0_ind1 = 0
            sum45_ind1 = 0
            sum90_ind1 = 0
            sum135_ind1 = 0

            sum0_ind2 = 0
            sum45_ind2 = 0
            sum90_ind2 = 0
            sum135_ind2 = 0

            for j in xrange(choice_N/4): # assumed 4 angles
                sum0_ind1 += z[ind1][j*4]
                sum45_ind1 += z[ind1][j*4+1]
                sum90_ind1 += z[ind1][j*4+2]
                sum135_ind1 += z[ind1][j*4+3]

            m.addConstr(z[ind1][0]+z[ind2][2]<=1.0)
            m.addConstr(z[ind1][1]+z[ind2][3]<=1.0)

        # neighborhood set constraint
    if (not neighborSet==None):
        for i in xrange(len(neighborSet)):
            ind_center = neighborSet[i][0]
            ind_neighborSet = neighborSet[i][1]

            sum_center = 0.0
            for ii in xrange(choice_N):
                sum_center += z[ind_center][ii]

            sum_neighbor = 0.0
            for j in xrange(len(ind_neighborSet)):
                ind_neighbor = ind_neighborSet[j]
                for ii in xrange(choice_N):
                    sum_neighbor += z[ind_neighbor][ii]

            m.addConstr(sum_center <= sum_neighbor)

    # monotone column constraint
    if (not colSet==None):
        sum_col_list = []
        for i in xrange(len(colSet)):

            ind_colSet = colSet[i]

            sum_col = 0.0
            for j in xrange(len(ind_colSet)):
                for ii in xrange(choice_N):

                    sum_col += z[ind_colSet[j]][ii]

            sum_col_list.append(sum_col)

            # no void column constraint
            # m.addConstr(sum_col >= 1.0)

        # monotone column constraint
        for i in xrange(len(sum_col_list)-1):

            m.addConstr(sum_col_list[i]>=sum_col_list[i+1])

    if (not patchedElem_list==None):
        for i in xrange(len(patchedElem_list)):
            patchedElem_list_loc = patchedElem_list[i]
            for j in xrange(len(patchedElem_list_loc)-1):
                for k in xrange(choice_N):
                    m.addConstr(z[patchedElem_list_loc[j + 1]][k] == z[patchedElem_list_loc[0]][k])

    if (not Threads == None):
        m.params.Threads = Threads

    # Heuristic time
    m.params.Heuristics = 0.05 # default 0.05

    if flagCPLEXGenerator:
        m.write(jobName + '_DCC_old.mps')

        return None, None
    else:
        start = time.clock()
        m.optimize()
        end = time.clock()
        opt_time = end - start

        var_name_list = []
        var_value_list = []
        for vv in m.getVars():

            var_name_list.append(vv.varName)
            var_value_list.append(vv.x)

        outDirBase = "Output/DCC_dense/"
        outDir = "./" + outDirBase
        os.makedirs(outDirBase)
        out_file_name = outDir + jobName + '.txt'

        with open(out_file_name, "w") as outfp:
            for i in xrange(len(var_name_list)):
                outfp.write('%s %.6f\n' %(var_name_list[i], var_value_list[i]))


        return var_name_list, var_value_list



