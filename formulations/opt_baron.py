import os
import sys

cwd = os.getcwd()
sys.path.append(cwd + '/utils') 

import numpy as np
import time
from pyomo.environ import *
from plate_layerwise import *
import copy



def opt_baron(K_option_list, \
    Pj_option_list, qj_option_list, Qj_option_list,\
    B_option_list, \
    node, elem, BC, f,\
    lb, ub, \
    conn = None, neighborSet=None, colSet=None, patchedElem_list=None, Threads=None, \
    checkerboardSet = None, FlagWeight = True, totalWeight = None):
    

    dof_per_node = 5
    choice_N = len(K_option_list)
    node_N = len(node)
    elem_N = len(elem)

    # ==============
    #  Optimization                         
    # ==============

    # model
    model = m = ConcreteModel()

    # --------------
    # add variables
    # --------------
    # choice variables
    elemIndList = [i for i in range(elem_N)]
    choiceIndList = [i for i in range(choice_N)]
    strainIndList = [i for i in range(3)]
    nodeIndList = [i for i in range(node_N)]
    dofIndList = [i for i in range(dof_per_node)]
    
    m.z = Var(elemIndList, choiceIndList, within=Binary)
    # m.z[0, 0] = 1
    # m.z[2, 0] = 1

    # displacement variables
    m.setNode = Set(initialize = nodeIndList)
    m.setDof = Set(initialize = dofIndList)
    m.setU = m.setNode * m.setDof
    def fb(model, i, j):
        return (lb[i][j], ub[i][j])
    m.u = Var(m.setU, within=Reals, bounds=fb)

    # pseudo strain
    m.epsilon = Var(elemIndList, choiceIndList, strainIndList, within=Reals)

    # --------------
    # add objective
    # --------------
    if FlagWeight:
        def mass(m):
            return sum(m.z[i, j] for i in elemIndList for j in choiceIndList)

        m.obj = Objective(rule=mass)
    else:
        def compliance(m):
            compliance = 0.0
            for i in xrange(node_N):
                if(i not in BC):
                    for j in xrange(dof_per_node):

                        index_loc = i*dof_per_node + j
                        compliance += f[index_loc] * m.u[i, j]
            return compliance

        m.obj = Objective(rule=compliance)

    # f = m.z[0, 0] + m.z[0, 1]
    # m.one_per_cust = Constraint(expr = f >= 1)
    # --------------
    # add constraints
    # --------------
    # weight constrain (if applicable)
    if (not FlagWeight):
        # weight constraint   
        m.massConstr = ConstraintList()

        glo_sum_z = 0.0
        for i in xrange(elem_N):
            for j in xrange(choice_N):
                glo_sum_z += m.z[i, j]

        m.massConstr.add(expr = glo_sum_z <= totalWeight)



    # equilibrium constraints
    f_sum = np.zeros(node_N*dof_per_node).tolist()

    for i in range(elem_N):

        # at first, for each element we collect the force into a local array
        element2Node_loc = elem[i]

        f_loc = np.zeros(9*dof_per_node).tolist()
        for j in range(choice_N):

            Kij = K_option_list[j]

            for ii in range(9):
                for iii in range(dof_per_node):

                    index_dof_i_local = ii * dof_per_node + iii

                    for jj in range(9):

                        index_node_j_global = element2Node_loc[jj]

                        for jjj in range(dof_per_node):
                            
                            index_dof_j_local = jj * dof_per_node + jjj

                            f_loc[index_dof_i_local] += Kij[index_dof_i_local, index_dof_j_local] * \
                            m.u[index_node_j_global, jjj] * m.z[i, j]

        # then we map the previous local array to the global array
        for ii in range(9): 

            glo_index = element2Node_loc[ii]

            for jj in range(dof_per_node):

                f_sum[glo_index*dof_per_node+jj] += f_loc[ii*dof_per_node+jj]

    # we write the equality constraint between the global reaction force 
    # we have just assembled into the global array with the external force.
    # Except for the element on the boundary.
    # ??? How to define an array constraint???
    m.equilibrium = ConstraintList()
    for i in range(node_N):
        if(i not in BC):
            for j in range(dof_per_node):

                index_loc = i*dof_per_node + j
                m.equilibrium.add(expr = f_sum[index_loc] == f[index_loc])

    # BC constraints 
    m.BC = ConstraintList()
    for ind_BC_loc in BC:
        for j in range(dof_per_node):

            m.BC.add(expr = m.u[ind_BC_loc, j] == 0.0)

    # strain-disp constraints
    m.strainDisp = ConstraintList()
    for i in range(elem_N):

        element2Node_loc = elem[i]

        for j in range(choice_N): 

            B_loc = B_option_list[j]

            for ii in range(3):

                epsilon_loc = 0.0

                for jj in range(9):

                    index_node_global = element2Node_loc[jj]

                    for kk in range(dof_per_node):

                        index_dof_y_local = jj * dof_per_node + kk
                        epsilon_loc += B_loc[ii, index_dof_y_local] * m.u[index_node_global, kk] * m.z[i, j]

                m.strainDisp.add(expr = epsilon_loc == m.epsilon[i, j, ii])

    # Tsai-Wu stress constraint
    m.tsaiWu = ConstraintList()
    for i in range(elem_N):
        for j in range(choice_N):

            Pj_loc = Pj_option_list[j]
            qj_loc = qj_option_list[j]

            Qj_loc = Qj_option_list[j]

            QjTPjQj = np.transpose(Qj_loc).dot(Pj_loc).dot(Qj_loc)
            QjTqj = np.transpose(Qj_loc).dot(qj_loc)

            sum_tsai = 0.0
            for ii in range(3):
                for jj in range(3):
                    sum_tsai += QjTPjQj[ii, jj] * m.epsilon[i, j, ii] * m.epsilon[i, j, jj]

            for ii in range(3):
                sum_tsai += 2 * QjTqj[ii, 0] * m.epsilon[i, j, ii]

            sum_tsai += -1.0

            m.tsaiWu.add(expr = sum_tsai <= 0.0)

    
    # topology constraint
    m.topo = ConstraintList()
    for i in range(elem_N):

        sum_z = 0.0

        for j in range(choice_N):
            sum_z += m.z[i, j]

        m.topo.add(expr = sum_z <= 1.0)


    # checkerboard constraint
    m.checkerboard = ConstraintList()
    if (not checkerboardSet == None):
        for i in range(len(checkerboardSet)):

            # ind2 | ind4
            # -----------
            # ind1 | ind3

            ind1, ind2, ind3, ind4 = checkerboardSet[i][:]

            z1 = 0.0
            z2 = 0.0
            z3 = 0.0
            z4 = 0.0
            for j in range(choice_N):
                z1 += m.z[ind1, j]
                z2 += m.z[ind2, j]
                z3 += m.z[ind3, j]
                z4 += m.z[ind4, j]

            m.checkerboard.add(expr = (z1 + z4) - (z2 + z3) <=  1.0)
            m.checkerboard.add(expr = (z1 + z4) - (z2 + z3) >= -1.0)

    # manufacturing constraint
    m.manufacture = ConstraintList()
    if (not conn == None):
        for i in range(len(conn)):

            ind1 = conn[i][0]
            ind2 = conn[i][1]

            m.manufacture.add(expr = m.z[ind1, 0] + m.z[ind2, 2] <= 1.0)
            m.manufacture.add(expr = m.z[ind1, 1] + m.z[ind2, 3] <= 1.0)

    solver = SolverFactory('baron')
    solver.options['lpsol'] = '3'
    solver.options['CplexLibName'] = "/Applications/CPLEX_Studio1210/cplex/bin/x86-64_osx/libcplex12100.dylib"
    solver.options['summary'] = '1'
    solver.options['threads'] = str(Threads)
    solver.options['MaxTime'] = '-1'

    ts = time.time()
    solver.solve(m, tee='stream_solver')
    te = time.time()

    # print(solver)
    # m.pprint()
    m.obj.pprint()
    m.z.pprint()
    m.u.pprint()
    m.epsilon.pprint()
    m.tsaiWu.display()

    print("T: ", te - ts)