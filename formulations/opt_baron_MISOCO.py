import os
import sys

cwd = os.getcwd()
sys.path.append(cwd + '/utils') 

import numpy as np
import time
from pyomo.environ import *
from plate_layerwise import *
import copy



def opt_baron_MISOCO(K_option_list, \
    Pj_option_list, qj_option_list, Qj_option_list,\
    B_option_list, \
    node, elem, BC, f,\
    lb, ub, \
    conn = None, neighborSet=None, colSet=None, patchedElem_list=None, Threads=None, \
    checkerboardSet = None, FlagWeight = True, totalWeight = None,
    formulation = None):
    

    flag_le = False
    flag_se = False
    flag_DCC_sparse = False
    flag_DCC_dense = False
    if formulation.lower() == "le":
        flag_le = True
    elif formulation.lower() == "se":
        flag_se = True
    elif formulation.lower() == "dcc_sparse":
        flag_DCC_sparse = True
    elif formulation.lower() == "dcc_dense":
        flag_DCC_dense = True

    dof_per_node = 5
    choice_N = len(K_option_list)
    node_N = len(node)
    elem_N = len(elem)

    # ==============
    #  Optimization                         
    # ==============

    # model
    model = m = ConcreteModel()

    # --------------
    # add variables
    # --------------
    # choice variables
    elemIndList = [i for i in range(elem_N)]
    choiceIndList = [i for i in range(choice_N)]
    strainIndList = [i for i in range(3)]
    nodeIndList = [i for i in range(node_N)]
    dofIndList = [i for i in range(dof_per_node)]
    nodePerElemList = [i for i in range(9)]

    # define seveval sets
    m.setElem = Set(initialize = elemIndList)
    m.setChoice = Set(initialize = choiceIndList)
    m.setStrainPlus1 = Set(initialize = [i for i in range(4)])
    m.setNode = Set(initialize = nodeIndList)
    m.setDof = Set(initialize = dofIndList)
    
    m.z = Var(elemIndList, choiceIndList, within=Binary)
    # m.z[0, 0] = 1
    # m.z[2, 0] = 1

    # displacement variables
    m.setU = m.setNode * m.setDof
    def fb(model, i, j):
        return (lb[i][j], ub[i][j])
    m.u = Var(m.setU, within=Reals, bounds=fb)

    # pseudo displacement
    m.psi = Var(elemIndList, choiceIndList, nodePerElemList, dofIndList, within=Reals)

    # pseudo strain
    m.epsilon = Var(elemIndList, choiceIndList, strainIndList, within=Reals)

    if flag_DCC_sparse:

        m.setY = m.setElem * m.setChoice
        def fb_y(model, i, j):
            return (0.0, float('inf'))

        m.x = Var(elemIndList, choiceIndList, strainIndList, within=Reals)
        m.y = Var(m.setY, within=Reals, bounds = fb_y)

    if flag_DCC_dense:
        
        m.setV = m.setElem * m.setChoice * m.setStrainPlus1
        def fb_V(model, i, j, k):
            if k == 0:
                return [0, float('inf')]
            else:
                return [float('-inf'), float('inf')]
        m.v = Var(m.setV, within=Reals, bounds=fb_V)

    # --------------
    # add objective
    # --------------
    if FlagWeight:
        def mass(m):
            return sum(m.z[i, j] for i in elemIndList for j in choiceIndList)

        m.obj = Objective(rule=mass)
    else:
        def compliance(m):
            compliance = 0.0
            for i in xrange(node_N):
                if(i not in BC):
                    for j in xrange(dof_per_node):

                        index_loc = i*dof_per_node + j
                        compliance += f[index_loc] * m.u[i, j]
            return compliance

        m.obj = Objective(rule=compliance)

    # f = m.z[0, 0] + m.z[0, 1]
    # m.one_per_cust = Constraint(expr = f >= 1)
    # --------------
    # add constraints
    # --------------
    # weight constrain (if applicable)
    if (not FlagWeight):
        # weight constraint   
        m.massConstr = ConstraintList()

        glo_sum_z = 0.0
        for i in xrange(elem_N):
            for j in xrange(choice_N):
                glo_sum_z += m.z[i, j]

        m.massConstr.add(expr = glo_sum_z <= totalWeight)

    # Petersen
    m.Petersen = ConstraintList()
    for i in xrange(elem_N):

        elem_loc = elem[i]

        for j in xrange(choice_N):
            for k in xrange(9):

                glo_index = elem_loc[k]

                for ii in xrange(dof_per_node):
                    m.Petersen.add(expr = m.psi[i, j, k, ii] >= lb[glo_index][ii] * m.z[i, j])
                    m.Petersen.add(expr = m.psi[i, j, k, ii] <= ub[glo_index][ii] * m.z[i, j])
                    m.Petersen.add(expr = m.psi[i, j, k, ii] >= ub[glo_index][ii] * (m.z[i, j] - 1.0) + m.u[glo_index, ii])
                    m.Petersen.add(expr = m.psi[i, j, k, ii] <= lb[glo_index][ii] * (m.z[i, j] - 1.0) + m.u[glo_index, ii])

    # equilibrium constraints
    f_sum = np.zeros(node_N * dof_per_node).tolist()

    for i in range(elem_N):

        # at first, for each element we collect the force into a local array
        element2Node_loc = elem[i]

        f_loc = np.zeros(9 * dof_per_node).tolist()
        for j in range(choice_N):

            Kij = K_option_list[j]

            for ii in range(9):
                for iii in range(dof_per_node):

                    index_dof_i_local = ii * dof_per_node + iii

                    for jj in range(9):

                        index_node_j_global = element2Node_loc[jj]

                        for jjj in range(dof_per_node):
                            
                            index_dof_j_local = jj * dof_per_node + jjj

                            f_loc[index_dof_i_local] += Kij[index_dof_i_local, index_dof_j_local] * m.psi[i, j, jj, jjj]

        # then we map the previous local array to the global array
        for ii in range(9): 

            glo_index = element2Node_loc[ii]

            for jj in range(dof_per_node):

                f_sum[glo_index*dof_per_node+jj] += f_loc[ii*dof_per_node+jj]

    # we write the equality constraint between the global reaction force 
    # we have just assembled into the global array with the external force.
    # Except for the element on the boundary.
    # ??? How to define an array constraint???
    m.equilibrium = ConstraintList()
    for i in range(node_N):
        if(i not in BC):
            for j in range(dof_per_node):

                index_loc = i*dof_per_node + j
                m.equilibrium.add(expr = f_sum[index_loc] == f[index_loc])

    # BC constraints 
    m.BC = ConstraintList()
    for ind_BC_loc in BC:
        for j in range(dof_per_node):

            m.BC.add(expr = m.u[ind_BC_loc, j] == 0.0)

    # strain-disp constraints
    m.strainDisp = ConstraintList()
    for i in range(elem_N):

        element2Node_loc = elem[i]

        for j in range(choice_N): 

            B_loc = B_option_list[j]

            for ii in range(3):

                epsilon_loc = 0.0

                for jj in range(9):

                    index_node_global = element2Node_loc[jj]

                    for kk in range(dof_per_node):

                        index_dof_y_local = jj * dof_per_node + kk
                        epsilon_loc += B_loc[ii, index_dof_y_local] * m.psi[i, j, jj, kk]

                m.strainDisp.add(expr = epsilon_loc == m.epsilon[i, j, ii])

    # Tsai-Wu stress constraint
    m.tsaiWu = ConstraintList()

    if flag_le:
        for i in range(elem_N):

            sum_tsai = 0.0

            for j in range(choice_N):

                Pj_loc = Pj_option_list[j]
                qj_loc = qj_option_list[j]

                Qj_loc = Qj_option_list[j]

                QjTPjQj = np.transpose(Qj_loc).dot(Pj_loc).dot(Qj_loc)
                QjTqj = np.transpose(Qj_loc).dot(qj_loc)

                for ii in range(3):
                    for jj in range(3):
                        sum_tsai += QjTPjQj[ii, jj] * m.epsilon[i, j, ii] * m.epsilon[i, j, jj]

                for ii in range(3):
                    sum_tsai += 2 * QjTqj[ii, 0] * m.epsilon[i, j, ii]

            sum_tsai += -1.0

            m.tsaiWu.add(expr = sum_tsai <= 0.0)

    elif flag_se:
        for i in range(elem_N):
            for j in range(choice_N):

                Pj_loc = Pj_option_list[j]
                qj_loc = qj_option_list[j]

                Qj_loc = Qj_option_list[j]

                QjTPjQj = np.transpose(Qj_loc).dot(Pj_loc).dot(Qj_loc)
                QjTqj = np.transpose(Qj_loc).dot(qj_loc)

                sum_tsai = 0.0
                for ii in range(3):
                    for jj in range(3):
                        sum_tsai += QjTPjQj[ii, jj] * m.epsilon[i, j, ii] * m.epsilon[i, j, jj]

                for ii in range(3):
                    sum_tsai += 2 * QjTqj[ii, 0] * m.epsilon[i, j, ii]

                sum_tsai += -1.0

                m.tsaiWu.add(expr = sum_tsai <= 0.0)

    elif flag_DCC_sparse:

        m.epsilon2x = ConstraintList()
        QPqqQ_option_list = []
        Qjqj_option_list = []
        for i in xrange(choice_N):

            Pj_loc = Pj_option_list[i]
            qj_loc = qj_option_list[i]
            Qj_loc = Qj_option_list[i]

            # Qj^T(Pj+qj*qj^T)Qj
            QPqqQ_loc = np.transpose(Qj_loc)\
            .dot(Pj_loc + qj_loc.dot(np.transpose(qj_loc)))\
            .dot(Qj_loc)

            QPqqQ_option_list.append(QPqqQ_loc)

            # Qj^Tqj
            Qjqj_loc = (np.transpose(Qj_loc)).dot(qj_loc)

            Qjqj_option_list.append(Qjqj_loc)

        # eigenvalue decomp: Qj^T(Pj+qj*qj^T)Qj = Uj*Lambda*Uj^T
        # then form sqrt(Lambda)*Uj^T which is used in:
        # x=sqrt(Lambda)*Uj^T*epsilon
        LambdaSqrtU_option_list = []
        for i in xrange(choice_N):

            QPqqQ_loc = QPqqQ_option_list[i]

            Lambda_loc, U_loc = np.linalg.eig(QPqqQ_loc)

            LambdaSqrt_loc = np.diag(np.sqrt(Lambda_loc))

            LambdaSqrtU_loc = LambdaSqrt_loc.dot(np.transpose(U_loc))

            LambdaSqrtU_option_list.append(LambdaSqrtU_loc)

        # x_{i,j}=(sqrt(Lambda_j)*U_j^T)*epsilon_{i,j}
        for i in xrange(elem_N):
            for j in xrange(choice_N):
                coeff_mat = LambdaSqrtU_option_list[j]
                for ii in xrange(3):
                    x_ij_ii = 0.0
                    for jj in xrange(3):
                        x_ij_ii += coeff_mat[ii, jj] * m.epsilon[i, j, jj]
                    m.epsilon2x.add(expr = x_ij_ii == m.x[i, j, ii])

        m.epsilonZ2x = ConstraintList()
        # y_{i,j} = -epsilon_{i,j}^TQ_j^Tq_j+z_{i,j}
        for i in xrange(elem_N):
            for j in xrange(choice_N):
                coeff_vec = Qjqj_option_list[j]

                y_ij = 0.0

                for ii in xrange(3):
                    y_ij += -coeff_vec[ii, 0] * m.epsilon[i, j, ii]

                y_ij += m.z[i, j]

                m.epsilonZ2x.add(y_ij == m.y[i, j])

        # cone constraint
        m.cone = ConstraintList()
        for i in xrange(elem_N):
            for j in xrange(choice_N):

                xTx = m.x[i, j, 0] * m.x[i, j, 0] \
                + m.x[i, j, 1] * m.x[i, j, 1] \
                + m.x[i, j, 2] * m.x[i, j, 2]

                y_2 = m.y[i, j] * m.y[i, j]

                m.cone.add(expr = xTx - y_2 <= 0.0)

    elif flag_DCC_dense:


        m.epsilonZ2v = ConstraintList()
        m.cone = ConstraintList()

        # Tsai-Wu stress constraints (with DCC)
        # Pj_option_list, qj_option_list, Qj_option_list
        QPQ_option_list = []

        for i in xrange(choice_N):

            Pj_loc = Pj_option_list[i]
            qj_loc = qj_option_list[i]
            Qj_loc = Qj_option_list[i]

            Pqqr = np.zeros((4, 4))
            Pqqr[:3, :3] += Pj_loc
            Pqqr[:3, 3:] += qj_loc
            Pqqr[3:, :3] += np.transpose(qj_loc)
            Pqqr[3, 3] = -1.0

            Q1 = np.zeros((4, 4))
            Q1[:3, :3] += Qj_loc
            Q1[3, 3] = 1.0

            QPQ_loc = np.transpose(Q1).dot(Pqqr).dot(Q1)

            QPQ_option_list.append(QPQ_loc)


        for i in xrange(elem_N):
            for j in xrange(choice_N):

                QPQ_loc = QPQ_option_list[j]

                [eig_val, P] = np.linalg.eig(QPQ_loc)
                eig_val = eig_val.tolist()

                ind_neg = eig_val.index(min(eig_val))

                if not ind_neg == 0:

                    P0 = copy.deepcopy(P[:, 0])
                    Pneg = copy.deepcopy(P[:, ind_neg])

                    eig0 = eig_val[0]
                    eigneg = eig_val[ind_neg]

                    # switch them!

                    P[:, 0] = Pneg
                    P[:, ind_neg] = P0

                    eig_val[0] = eigneg
                    eig_val[ind_neg] = eig0

                # make sure the right branch of cone picked
                if P[-1, 0] <= 0.0:

                    P[:, 0] = -P[:, 0]


                # get the square root of absolute value of the eigenvalues
                abs_eig_val = np.diag(np.abs(copy.deepcopy(eig_val)))

                root_abs_eig_val = np.zeros((4, 4))
                for ii in xrange(4):
                    root_abs_eig_val[ii, ii] = np.sqrt(abs_eig_val[ii, ii])

                lambda_P = root_abs_eig_val.dot(np.transpose(P))

                # constraibtns for intermediate vars -- v:
                for ii in xrange(4):

                    sum_prod = 0.0

                    for jj in xrange(3):
                        sum_prod += lambda_P[ii, jj] * m.epsilon[i, j, jj]

                    sum_prod += lambda_P[ii, 3] * m.z[i, j]

                    m.epsilonZ2v.add(sum_prod == m.v[i, j, ii])


                # add the cone constraints
                
                sum_prod = 0.0
                for ii in xrange(3):

                    sum_prod += m.v[i, j, ii+1] * m.v[i, j, ii+1]

                m.cone.add(expr = sum_prod - m.v[i, j, 0] * m.v[i, j, 0] <= 0.0)


    # topology constraint
    m.topo = ConstraintList()
    for i in range(elem_N):

        sum_z = 0.0

        for j in range(choice_N):
            sum_z += m.z[i, j]

        m.topo.add(expr = sum_z <= 1.0)


    # checkerboard constraint
    m.checkerboard = ConstraintList()
    if (not checkerboardSet == None):
        for i in range(len(checkerboardSet)):

            # ind2 | ind4
            # -----------
            # ind1 | ind3

            ind1, ind2, ind3, ind4 = checkerboardSet[i][:]

            z1 = 0.0
            z2 = 0.0
            z3 = 0.0
            z4 = 0.0
            for j in range(choice_N):
                z1 += m.z[ind1, j]
                z2 += m.z[ind2, j]
                z3 += m.z[ind3, j]
                z4 += m.z[ind4, j]

            m.checkerboard.add(expr = (z1 + z4) - (z2 + z3) <=  1.0)
            m.checkerboard.add(expr = (z1 + z4) - (z2 + z3) >= -1.0)

    # manufacturing constraint
    m.manufacture = ConstraintList()
    if (not conn == None):
        for i in range(len(conn)):

            ind1 = conn[i][0]
            ind2 = conn[i][1]

            m.manufacture.add(expr = m.z[ind1, 0] + m.z[ind2, 2] <= 1.0)
            m.manufacture.add(expr = m.z[ind1, 1] + m.z[ind2, 3] <= 1.0)

    solver = SolverFactory('baron')
    solver.options['lpsol'] = '3'
    solver.options['CplexLibName'] = "/Applications/CPLEX_Studio1210/cplex/bin/x86-64_osx/libcplex12100.dylib"
    solver.options['summary'] = '1'
    solver.options['threads'] = str(Threads)
    solver.options['MaxTime'] = '-1'

    ts = time.time()
    solver.solve(m, tee='stream_solver')
    te = time.time()

    # print(solver)
    # m.pprint()
    m.obj.pprint()
    m.z.pprint()
    m.u.pprint()
    m.epsilon.pprint()
    m.tsaiWu.display()

    print("T: ", te - ts)